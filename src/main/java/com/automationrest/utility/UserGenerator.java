package com.automationrest.utility;

import com.automationrest.model.ApiUser;
import com.github.javafaker.Faker;

public class UserGenerator {

    private final static Faker faker = new Faker();

    public static ApiUser generateWithValidCredentials() {
        return ApiUser.builder()
                .id(faker.random().nextInt(32))
                .email(faker.internet().emailAddress())
                .firstName(faker.name().firstName())
                .lastName(faker.name().lastName())
                .phone(faker.phoneNumber().phoneNumber())
                .username(faker.name().username())
                .password(faker.internet().password(6,9))
                .userStatus(1)
                .build();
    }
}

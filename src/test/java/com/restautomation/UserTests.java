package com.restautomation;

import com.automationrest.model.ApiUser;
import com.automationrest.model.UserResponse;
import com.automationrest.utility.UserGenerator;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

class UserTests extends BaseTest {

    @Test
    void createUser() {
        ApiUser newUser = UserGenerator.generateWithValidCredentials();

        UserResponse user =
                given()
                        .header("accept", " application/json")
                        .header("Content-Type", "application/json")
                        .body(newUser).
                when()
                        .post("https://petstore.swagger.io/v2/user").
                then()
                        .extract().as(UserResponse.class);

        assertThat(user.getCode()).isEqualTo(200);
    }
}

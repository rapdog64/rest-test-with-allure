package com.restautomation;

import com.automationrest.ApiUrls;
import io.restassured.RestAssured;
import org.junit.jupiter.api.BeforeAll;

import static com.restautomation.config.CustomLogFilter.customLogFilter;

public class BaseTest {

    @BeforeAll
    public static void setUp() {
        RestAssured.baseURI = ApiUrls.BASE_URL;
        RestAssured.filters(customLogFilter().withCustomTemplates());
    }
}
